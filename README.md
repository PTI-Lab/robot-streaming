# ROBOT STREAMING #

### What is this repository for? ###

This repository contains the source files for the streaming application in Android. It basically streams from the smartphone camera to a server. You can choose the video resolution as well as the front or back camera.

It uses an opensource library for the streaming called **[libstreaming](https://github.com/fyhertz/libstreaming)**.

### How do I get set up? ###

1. We need to have Android Studio in our machine. You can download it from [here](http://developer.android.com/intl/es/sdk/index.html)
2. We also need to have our Android SDK set up for API 21 (IT MUST BE version 21!)
3. Clone the repository: **git clone https://rioardila@bitbucket.org/PTI-Lab/robot-streaming.git** 
4. Start Android Studio and import the Android Studio project
5. Make sure again you have Android API v. 21, then you can build the project and test it
